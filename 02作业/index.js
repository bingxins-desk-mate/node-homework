const http =require('http')
const dayjs =require('dayjs')
const db=require('./db.js')
const server = http.createServer()
server.listen(8848, () => {
    console.log('my server start work')
})

server.on('request', (request, response) => {
    let { url, method } = request
    let data = {}
    if (method.toLowerCase() === 'get') {
        if (url === '/api/news') {
            data = db.news()
            data.forEach(t=>{
                t.publish_time = dayjs(t.publish_time).format('YYYY-MM-DD HH:mm:ss')
            })
        }else if (url === '/api/students') {
            data = db.students()
        } else if (url === '/api/lucystar') {
            data = db.lucystar()
        }
    }
    response.setHeader('Content-Type','application/json; charset=utf-8')
    response.setHeader('Access-Control-Allow-Origin','*')
    response.end(JSON.stringify(data))
})
