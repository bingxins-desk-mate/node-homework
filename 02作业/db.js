const fs =require('fs')
const path =require('path')

const news = function () {
    let filePath = path.join(__dirname, './data/news.json')
    let str = fs.readFileSync(filePath, 'utf-8')
    return JSON.parse(str)
}
const students = function () {
    let filePath = path.join(__dirname, './data/info.json')
    let str = fs.readFileSync(filePath, 'utf-8')
    return JSON.parse(str)
}
const lucystar = function () {
    let list = students()
    let i = Math.floor(Math.random() * list.length)
    list[i].count++
    let filePath = path.join(__dirname, './data/info.json')
    fs.writeFileSync(filePath, JSON.stringify(list))
    return list[i]
}
module.exports={
    news,
    students,
    lucystar
}